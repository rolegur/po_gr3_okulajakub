package pl.imiajd.Okula;

import java.time.LocalDate;

public class Skrzypce extends Instrument {

    public Skrzypce(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);
    }
    public void Dzwiek(){
        System.out.println("Dzwiek skrzypiec");
    }

    @Override
    public String toString() {
        return "Skrzypce{" + "producent=" + producent + ", rokProdukcji=" + rokProdukcji + '}';
    }
    
}
