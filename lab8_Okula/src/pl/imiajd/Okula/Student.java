
package pl.imiajd.Okula;

import java.time.LocalDate;
import java.util.Arrays;


public class Student extends Osoba {
    String kierunek;
    double sredniaOcen;

    public Student(String kierunek, double sredniaOcen, String naz, int rok, String[] imie, LocalDate dataUr, boolean pl) {
        super(naz, rok, imie, dataUr, pl);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    @Override
    public String toString() {
        return "Student{" + "kierunek=" + kierunek + ", sredniaOcen=" + sredniaOcen + " nazwisko=" + nazwisko + ", rok_urodzenia=" + rok_urodzenia + ", imiona=" + Arrays.toString(imiona) + ", dataUrodzenia=" + dataUrodzenia + ", plec=" + plec + '}';
    }
    
   

    public String getKierunek() {
        return kierunek;
    }

    public double getSredniaOcen() {
        return sredniaOcen;
    }

    public void setSredniaOcen(double sredniaOcen) {
        this.sredniaOcen = sredniaOcen;
    }



    
    
}
