package pl.imiajd.Okula;

import java.time.LocalDate;

public class Flet extends Instrument{
    
    public void Dzwiek(){
        System.out.println("Dzwiek fletu");
    }

    public Flet(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);
    }

    @Override
    public String toString() {
        return "Flet{" + "producent=" + producent + ", rokProdukcji=" + rokProdukcji + '}';
    }
    
}
