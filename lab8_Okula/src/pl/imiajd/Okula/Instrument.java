
package pl.imiajd.Okula;

import java.time.LocalDate;
import java.util.Objects;


public abstract class Instrument {
    String producent;
    java.time.LocalDate rokProdukcji;
    
    public Instrument(String producent, LocalDate rokProdukcji) {
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }

    public String getProducent() {
        return producent;
    }

    public LocalDate getRokProdukcji() {
        return rokProdukcji;
    }
    
    public abstract void Dzwiek();

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Instrument other = (Instrument) obj;
        if (!Objects.equals(this.producent, other.producent)) {
            return false;
        }
        if (!Objects.equals(this.rokProdukcji, other.rokProdukcji)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Instrument{" + "producent=" + producent + ", rokProdukcji=" + rokProdukcji + '}';
    }
            
    
}
