
package pl.imiajd.Okula;

import java.time.LocalDate;
import java.util.Arrays;


public class Osoba {
    String nazwisko;
    int rok_urodzenia;
    String[] imiona;
    java.time.LocalDate dataUrodzenia;
    boolean plec;
    
    public Osoba(String naz, int rok, String[] imie, java.time.LocalDate dataUr, boolean pl){
        nazwisko = naz;
        rok_urodzenia = rok;
        imiona = imie;
        dataUrodzenia = dataUr;
        plec = pl;
    }

    @Override
    public String toString() {
        return "Osoba{" + "nazwisko=" + nazwisko + ", rok_urodzenia=" + rok_urodzenia + ", imiona=" + Arrays.toString(imiona) + ", dataUrodzenia=" + dataUrodzenia + ", plec=" + plec + '}';
    }


    public String getNazwisko() {
        return nazwisko;
    }

    public int getRok_urodzenia() {
        return rok_urodzenia;
    }

    public String[] getImiona() {
        return imiona;
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    public boolean isPlec() {
        return plec;
    }
    
}
