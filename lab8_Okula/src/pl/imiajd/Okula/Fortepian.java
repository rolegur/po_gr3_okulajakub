package pl.imiajd.Okula;

import java.time.LocalDate;

public class Fortepian extends Instrument{

    public Fortepian(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);
    }
    
    public void Dzwiek(){
        System.out.println("Dzwiek fortepianu");
    }

    @Override
    public String toString() {
        return "Fortepian{" + "producent=" + producent + ", rokProdukcji=" + rokProdukcji + '}';
    }
    
}
