
package pl.imiajd.Okula;

import java.time.LocalDate;
import java.util.Arrays;

public class Nauczyciel extends Osoba {
    int pensja;
    java.time.LocalDate dataZatrudnienia;

    public Nauczyciel(int pensja, LocalDate dataZatrudnienia, String naz, int rok, String[] imie, LocalDate dataUr, boolean pl) {
        super(naz, rok, imie, dataUr, pl);
        this.pensja = pensja;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    @Override
    public String toString() {
        return "Nauczyciel{" + "pensja=" + pensja + ", dataZatrudnienia=" + dataZatrudnienia + " nazwisko=" + nazwisko + ", rok_urodzenia=" + rok_urodzenia + ", imiona=" + Arrays.toString(imiona) + ", dataUrodzenia=" + dataUrodzenia + ", plec=" + plec + '}';
    }
    
  

    public int getPensja() {
        return pensja;
    }

    public LocalDate getDataZatrudnienia() {
        return dataZatrudnienia;
    }

    
    
}
