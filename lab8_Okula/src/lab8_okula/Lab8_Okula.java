package lab8_okula;

import pl.imiajd.Okula.Osoba;
import pl.imiajd.Okula.Nauczyciel;
import pl.imiajd.Okula.Student;
import java.time.LocalDate;


public class Lab8_Okula {

    
    public static void main(String[] args) {
        String[] imiona = {"Kowal","kowalski"};
        String[] imiona2 = {"KDSAKD", "sfsa"};
        String[] imiona3 = {"KDSAKD", "sfsa"};

        double sr = 4.0;
        Osoba osoba = new Osoba("Jan", 2000,imiona , LocalDate.now(), false);
        Student student = new Student("Mat", sr,"Janek", 2002, imiona2, LocalDate.now(), false); 
        Nauczyciel nauczyciel = new Nauczyciel(10000,LocalDate.now(), "Ewelina", 1990,imiona3,LocalDate.now(),true);
        System.out.println(osoba);
        System.out.println(student);
        student.setSredniaOcen(5.0);
        System.out.println(student);
        System.out.println(nauczyciel);
        
    }
    
}
