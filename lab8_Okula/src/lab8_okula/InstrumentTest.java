package lab8_okula;

import pl.imiajd.Okula.Instrument;
import pl.imiajd.Okula.Fortepian;
import pl.imiajd.Okula.Flet;
import pl.imiajd.Okula.Skrzypce;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class InstrumentTest {
    
    public static void main(String[] args) {
        ArrayList<Instrument> Orkiestra = new ArrayList<Instrument>();
        Fortepian fortepian = new Fortepian("Yamaha", LocalDate.now());
        Skrzypce skrzypce1 = new Skrzypce("Stentor", LocalDate.now());
        Skrzypce skrzypce2 = new Skrzypce("Ars Nova", LocalDate.now());
        Flet flet1 = new Flet("Miyazawa",LocalDate.now());
        Flet flet2 = new Flet("ANDREAS EASTMAN",LocalDate.now());
        Collections.addAll(Orkiestra,fortepian,skrzypce1,skrzypce2,flet1,flet2);
        System.out.println(Orkiestra.toString());
        for(Instrument i: Orkiestra){
        i.Dzwiek();
        }
    }
    
}
