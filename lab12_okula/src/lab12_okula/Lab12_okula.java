
package lab12_okula;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;


public class Lab12_okula {

   
    public static void main(String[] args) {
        LinkedList<String> pracownicy = new LinkedList<String>();
    pracownicy.add("Kowal");
    pracownicy.add("Janek");
    pracownicy.add("Geralt");
    pracownicy.add("Hehe");
    System.out.println(pracownicy);
    redukuj(pracownicy,3);
    System.out.println(pracownicy);
    odwroc_2(pracownicy);
    System.out.println(pracownicy);
    odwroc(pracownicy);
    System.out.println(pracownicy);
    dzielenie(492);
    sito(20);
    }
    
        
    public static<T> void redukuj(LinkedList<T> pracownicy,int n)
    {
        int licz=1;
        for(int i =0; i<pracownicy.size();i++){
            if (licz == n){
                pracownicy.remove(i);
                licz=1;
            }
            licz++;
        }
    }
    
    
    public static<T> void odwroc(LinkedList<T> lista){
            Collections.reverse(lista);
        }
    
    public static<T> void odwroc_2(LinkedList<T> lista){
            for (int i = 0; i < lista.size() / 2; i++) {
                T temp = lista.get(i);
                lista.set(i, lista.get(lista.size() - i - 1));
                lista.set(lista.size() - i - 1, temp);
            }
        }
    
    public static void dzielenie(int n){
        int temp;
        int t;
        Stack<Integer> stack = new Stack<Integer>();
            while(n != 0){
                temp = n%10;
                stack.push(temp);
                n=n/10;
                
            }
            while(stack.empty() != true){
                t = stack.pop();
                System.out.println(t);         
            }
                
        }
    
    
    
    public static void sito(int n){
        LinkedList<Integer> lista = new LinkedList<Integer>();
        LinkedList<Integer> primes = new LinkedList<Integer>();
         for (int i = 0; i < n; i++) {
             lista.add(i);
         }
         for (int i = 2; i < n; i++) {
             primes.add(i);
         }
         System.out.println(primes);
         for (int j =0;j<Math.sqrt(n);j++) {
             
             for(int i =0; i < primes.size(); i++){
             if(primes.get(i)!=primes.get(j) && primes.get(i)%primes.get(j)==0)
                 primes.remove(i);
             }
         }
         System.out.println(primes);
    
    }
    
    public static <T extends Iterable<E>, E> void print(T object){
        Iterator<E> iterator = object.iterator();
        while(iterator.hasNext()){
            System.out.print(iterator.next()+ ", ");
        }
    }
                
    
}
