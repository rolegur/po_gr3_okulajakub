package lab2_okula_zad2;

import java.util.Random;
import java.util.Scanner;


public class lab2_okula_zad2{
    
public static void main(String[] args) {
    int[] tab;
    Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb wylosowac");
        int n = scanner.nextInt();
        if(n<0 ||n>100){
          System.out.println("Z?e n");      
        }
        else{
            int minWartosc=-999;
            int maxWartosc=999;
            tab = new int[n];
            generuj(tab,n,minWartosc,maxWartosc);
            wyswietl(tab,n);
            int x= ileNieparzystych(tab);
            System.out.println("liczb nie parzystych jest " + x);
            int y= ileParzystych(tab);
            System.out.println("liczb parzystych jest " + y);
            System.out.println("liczb dodatnich jest " + ileDodatnich(tab));
            System.out.println("liczb ujemnych jest " + ileUjemnych(tab));
            System.out.println("Zer jest " + ileZerowych(tab));
            System.out.println("Elementow maksymalnych " + ileMaksymalnych(tab));
            System.out.println("Ciag dodatnikch " + dlugoscMaksymalnegoCiaguDodatnich(tab));
            signum(tab);
            wyswietl(tab,n);
            System.out.println("Podaj lewa");
            int lewa = scanner.nextInt();
            System.out.println("Podaj prawa");
            int prawa = scanner.nextInt();
            odwrocFragment(tab, lewa, prawa);
            wyswietl(tab,n);
            
          
        }
        }

    
    public static void generuj (int tab[],int n,int minWartosc,int maxWartosc){
        Random r = new Random();
        for(int i=0;i<n;i++){
            tab[i]=r.nextInt(maxWartosc - minWartosc + 1)+minWartosc;
        }
    }
    public static void wyswietl (int tab[],int n){
        for(int i=0;i<n;i++){
            System.out.println(tab[i]);
        }
    }
    public static int ileNieparzystych(int tab[]){
        int nieparzyste = 0;
        for(int i=0;i<tab.length;i++){   
            if(tab[i]%2!=0){
                nieparzyste+=1;
            }
        }
        return nieparzyste;
    }   
    public static int ileParzystych(int tab[]){
        int parzyste = 0;
        for(int i=0;i<tab.length;i++){   
            if(tab[i]%2==0){
                parzyste+=1;
            }
        }
        return parzyste;
    }  
    public static int ileDodatnich (int tab[]){
    int ile_dodatnich=0;
    for(int i=0;i<tab.length;i++){
            if(tab[i]>0){
                ile_dodatnich+=1;
            }
    }
    return ile_dodatnich;
    }
    public static int ileUjemnych (int tab[]){
         int ile_ujemnych=0;
         for(int i=0;i<tab.length;i++){
            if(tab[i]<0){
                ile_ujemnych+=1;
            }
    }
         return ile_ujemnych;
         
    }
    public static int ileZerowych (int tab[]){
         int ile_zero=0;
         for(int i=0;i<tab.length;i++){
            if(tab[i]==0){
                ile_zero+=1;
            }
    }
         return ile_zero;      
    }
    
    public static int ileMaksymalnych(int tab[]){
    int maks=tab[0];
        int ile_razy=0;
        for(int i=0;i<tab.length;i++){
            if(tab[i]>maks){
                maks=tab[i];
            }
            if(tab[i]!=maks){
            ile_razy=1;
            }
            else{
            ile_razy+=1;
            }
            }
        return ile_razy;
    }
    public static int sumaDodatnich (int tab[]){
        int ile_dodatnich=0;
        for(int i=0;i<tab.length;i++){
            if(tab[i]>0){
                ile_dodatnich+=1;
            }
        }
        return ile_dodatnich;
    }
    public static int sumaUjemnych (int tab[]){
        int ile_ujemnych=0;
        for(int i=0;i<tab.length;i++){
            if(tab[i]<0){
                ile_ujemnych+=1;
            }
        }
        return ile_ujemnych;
    }
    
    public static int dlugoscMaksymalnegoCiaguDodatnich (int tab[]){
    int ciag=0;
        int temp=0;
        for(int i=0;i<tab.length;i++){
            if(tab[i]>=0){
                ciag+=1;
            }
            else if (tab[i]<0){
                if(temp<ciag){
                    temp=ciag;
                    ciag=0;
                }
                else{
                    ciag=0;
                }
             
            } 
        }
        return temp;
    }
    
    public static void signum(int tab[]){
    for(int i=0;i<tab.length;i++){
            if(tab[i]>0){
                tab[i]=1;
            }
            else if (tab[i]<0){
                tab[i]=-1;
            }
        } 
    }
    public static void odwrocFragment(int tab[],int lewa,int prawa){
        int pom;
//        int rozm = (prawa-lewa)/2+lewa;
        for(int i=lewa;i<=prawa;i++)
        {
            pom=tab[i];
            tab[i]=tab[prawa];
            tab[prawa]=pom;
            prawa--;
        }
//        for(int i=lewa;i<=rozm;i++)
//        {
//            pom=tab[i];
//            tab[i]=tab[prawa];
//            tab[prawa]=pom;
//            prawa--;
//        }
    
    } 
}

