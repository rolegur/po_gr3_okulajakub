
package pl.imiajd.Okula;


public class Osoba {
    String nazwisko;
    int rok_urodzenia;
    
    public Osoba(String naz, int rok){
        nazwisko = naz;
        rok_urodzenia = rok;
    }

    @Override
    public String toString() {
        return "Osoba{" + "nazwisko=" + nazwisko + ", rok_urodzenia=" + rok_urodzenia + '}';
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public int getRok_urodzenia() {
        return rok_urodzenia;
    }
    
}
