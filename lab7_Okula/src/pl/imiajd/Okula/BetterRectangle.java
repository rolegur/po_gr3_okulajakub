
package lab7_okula;

import java.awt.Rectangle;


public class BetterRectangle extends Rectangle {
    
    public BetterRectangle(int width, int height){
        super(width,height);
        setLocation(0, 0);  
        setSize(width, height);
    
    }
    
    public int getArea(){
        int powierzchnia = super.width * super.height;
        return powierzchnia;
    }
    public int GetPerimeter(){
        int obwod = (2 * super.width) * (2 * super.height);
        return obwod;
    }
    
}
