
package pl.imiajd.Okula;

public class Nauczyciel extends Osoba {
    int pensja;
    
    public Nauczyciel(String naz, int rok,int p){
        super(naz,rok);
        pensja = p;
    }

    @Override
    public String toString() {
        return "Nauczyciel{" + "pensja=" + pensja + '}';
    }

    public int getPensja() {
        return pensja;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public int getRok_urodzenia() {
        return rok_urodzenia;
    }

    
    
}
