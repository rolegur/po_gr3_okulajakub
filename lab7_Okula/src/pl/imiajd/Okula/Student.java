
package pl.imiajd.Okula;


public class Student extends Osoba {
    String kierunek;
    
    public Student(String naz, int rok,String k){
        super(naz,rok);
        kierunek = k;
    }

    @Override
    public String toString() {
        return "Student{" + "kierunek=" + kierunek + '}';
    }

    public String getKierunek() {
        return kierunek;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public int getRok_urodzenia() {
        return rok_urodzenia;
    }

    
    
}
