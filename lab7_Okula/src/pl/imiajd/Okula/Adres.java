/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.imiajd.Okula;

/**
 *
 * @author local
 */
public class Adres{
        private String ulica;
        private String numer_domu;
        private String numer_mieszkania;
        private String miasto;
        private int kod_pocztowy;
        
        public Adres(String ul, String nr_d, String nr_m, String m, int k_pocz){
            ulica=ul;
            numer_domu=nr_d;
            numer_mieszkania=nr_m;
            miasto=m;
            kod_pocztowy=k_pocz;
        }
        public Adres(String ul, String nr_d,String m, int k_pocz){
            ulica=ul;
            numer_domu=nr_d;
            miasto=m;
            kod_pocztowy=k_pocz;
        }
        
        public void pokaz(){
              System.out.println(kod_pocztowy + " " + miasto);
              if (numer_mieszkania == null)
                  System.out.println(ulica + " " + numer_domu);
              else
                  System.out.println(ulica + " " + numer_domu + " " + numer_mieszkania);
        }
        public boolean przed(Adres drugie){
            if(this.kod_pocztowy > drugie.kod_pocztowy)
                return true;
            else
                return false;
        }
    }
