package lab7_okula;
   
import pl.imiajd.Okula.Student;
import pl.imiajd.Okula.Osoba;
import pl.imiajd.Okula.Nauczyciel;
import pl.imiajd.Okula.Adres;


public class Lab7_Okula {

    
    public static void main(String[] args) {
        Adres adres1 = new Adres("chopina","28","olsztyn",2000);
        Adres adres2 = new Adres("chopina","23","50","olsztyn",2004);
        adres1.pokaz();
        adres2.pokaz();
        System.out.println(adres2.przed(adres1));
        
        Osoba osoba = new Osoba("Jan", 2000);
        Student student = new Student("Jan",2000, "Mat");
        Nauczyciel nauczyciel = new Nauczyciel("Ewa",3000,900);
        
        Osoba osoba2 = new Student("Jan", 2000, "dsad");
        System.out.println(nauczyciel);
        System.out.println(student.getKierunek());
        
        BetterRectangle prostokat = new BetterRectangle(5,4);
        System.out.println("Pole: " + prostokat.getArea());
        System.out.println("Obwod: " + prostokat.GetPerimeter());
    }
    
}
