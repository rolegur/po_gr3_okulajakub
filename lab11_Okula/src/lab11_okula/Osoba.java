package lab11_okula;

import java.time.LocalDate;
import java.util.Objects;


public class Osoba implements Comparable<Osoba>,Cloneable{
    String nazwisko;
    java.time.LocalDate dataUrodzenia;
  
    
    public Osoba(String naz, java.time.LocalDate dataUr){
        nazwisko = naz;
        dataUrodzenia = dataUr;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    public void setDataUrodzenia(LocalDate dataUrodzenia) {
        this.dataUrodzenia = dataUrodzenia;
    }
    
    @Override
    public String toString() {
        return "Osoba[" + " " + nazwisko + ",  " + dataUrodzenia + ']';
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Osoba other = (Osoba) obj;
        if (!Objects.equals(this.nazwisko, other.nazwisko)) {
            return false;
        }
        if (!Objects.equals(this.dataUrodzenia, other.dataUrodzenia)) {
            return false;
        }
        return true;
    }

   
    
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
    
    
//    @Override
//    public int compareTo(Osoba o) { //wg dlugosci nazwsika
//        if(nazwisko == o.nazwisko){
//            return this.dataUrodzenia.compareTo(o.dataUrodzenia);
//        }else if(nazwisko.length() > o.nazwisko.length()){
//        return 1;
//        }else
//            return -1;
//    }
    @Override
    public int compareTo(Osoba o){ //wg alfabetu
        if(this.nazwisko.equals(o.nazwisko)){
            return this.dataUrodzenia.compareTo(o.dataUrodzenia);
        }
        else
            return this.nazwisko.compareTo(o.nazwisko);
    }

 }
    
    
    
    
    
    

    

