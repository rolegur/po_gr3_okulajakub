package lab11_okula.Pair;

import java.util.HashSet;
import java.util.Set;


public class PairUtil {
  
    public static<T> void swap(Pair<T> p){
        T temp = p.getFirst();   
        p.setFirst(p.getSecond());
        p.setSecond(temp);
    }

}
