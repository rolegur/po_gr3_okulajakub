package lab11_okula;


public class ArrayUtil<T extends Comparable<T>>{
    
    public static <T extends Comparable<T>> boolean isSorted(T[] array) {
    for (int i = 0; i < array.length - 1; ++i) {
        if (array[i].compareTo(array[i + 1]) > 0)
            return false;
    }
    return true;
}
    public static <T extends Comparable<T>> int binSearch(T[] array, T t) {
    int lewo = 0;
    int prawo = array.length - 1;
    int indeks;
     while (lewo <= prawo){
         indeks = (lewo+prawo)/2;
     if(array[indeks].compareTo(t) < 0)
         lewo = indeks+1;
     else if(array[indeks].compareTo(t) > 0)
         prawo = indeks-1;
     else
         return indeks;
     }
    return -1;
    }
    
    public static <T extends Comparable<T>> void selectionSort(T[] array){
        int min;
        for(int i=0; i<array.length-1;i++){
            min=i;
            for(int j=i+1;j<array.length ;j++) {
                if(array[j].compareTo(array[min]) < 0)
                    min = j;
            }
        T temp = array[i];
        array[i] = array[min];
        array[min] = temp;
        }  
    }  
    
    
    public static <T extends Comparable<T>> void mergeSort(T[] array){
    
    
    
    }
}

