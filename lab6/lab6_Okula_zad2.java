
package lab6_okula;

import java.util.Arrays;


class IntegerSet{
    private boolean[] tab;
    
    public IntegerSet() {
        tab = new boolean[101];
    }
       
     public static IntegerSet union(IntegerSet proba1, IntegerSet proba2){
        IntegerSet suma = new IntegerSet();
        for (int i = 1; i < 101; i++) {
            if(proba1.tab[i] || proba2.tab[i]){
                suma.tab[i] = true;
            }
        }
        return suma;
    }
    
    public static IntegerSet intersection(IntegerSet proba1, IntegerSet proba2){
        IntegerSet iloczyn = new IntegerSet();
        for (int i = 1; i < 101; i++) {
            if (proba1.tab[i] && proba2.tab[i]){
                iloczyn.tab[i] = true;
            }
        }

        return iloczyn;
    }
     public void insertElement(int element){
        for (int i = 1; i < 101; i++) {
            if(element == i){
                this.tab[i] = true;
            }
        }
    }
     public void deleteElement(int element){
        for (int i = 1; i < 101; i++) {
            if (element == i){
                this.tab[i] = false;
            }
        }
    }
    @Override
     public String toString() {
        StringBuilder s = new StringBuilder();
        for (int i = 1; i < 101; i++) {
            if(tab[i]){
                s.append(i).append(" ");
            }
        }

        return s.toString();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + Arrays.hashCode(this.tab);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IntegerSet other = (IntegerSet) obj;
        if (!Arrays.equals(this.tab, other.tab)) {
            return false;
        }
        return true;
    }
 
    
    
}
public class lab6_Okula_zad2 {
    public static void main(String[] args) {
        IntegerSet proba1 = new IntegerSet();
        IntegerSet proba2 = new IntegerSet();

        proba1.insertElement(2);
        proba1.insertElement(3);
        proba1.insertElement(5);
        System.out.println("Proba 1: " + proba1);
        
        proba2.insertElement(2);
        proba2.insertElement(8);
        proba2.insertElement(100);     
        System.out.println("Proba 2: " + proba2);

        IntegerSet sum = IntegerSet.union(proba1,proba2);
        System.out.println("Suma: " + sum);
        IntegerSet iloczyn = IntegerSet.intersection(proba1,proba2);
        System.out.println("Iloczyn: " + iloczyn);
    
    }
    
}
