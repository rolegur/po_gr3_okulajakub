/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab6_okula;

import java.math.BigDecimal;
import java.util.Scanner;

class RachunekBankowy {
  BigDecimal saldo;
  static double rocznaStopaProcentowa; 
    
  static BigDecimal obliczMiesieczneOdsetki(BigDecimal saldo){
      BigDecimal StopaProcentowa = new BigDecimal(rocznaStopaProcentowa);
      BigDecimal mnozenie=saldo.multiply(StopaProcentowa);
      BigDecimal wynik =mnozenie.divide(BigDecimal.valueOf(12),2);
      saldo = saldo.add(wynik);
      return saldo;    
  }
  
  static void setRocznaStopaProcentowa(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("rocznaStopaProcentowa");
        rocznaStopaProcentowa = scanner.nextDouble();
  }
}
public class Lab6_Okula {


    public static void main(String[] args) {
        RachunekBankowy saver1 = new RachunekBankowy();
        RachunekBankowy saver2 = new RachunekBankowy();
        saver1.saldo = new BigDecimal(2000.00);
        saver2.saldo = new BigDecimal(3000.00);
        System.out.println("Przed");
        System.out.println(saver1.saldo);
        System.out.println(saver2.saldo);
        RachunekBankowy.setRocznaStopaProcentowa();
        saver1.saldo=RachunekBankowy.obliczMiesieczneOdsetki(saver1.saldo);
        saver2.saldo=RachunekBankowy.obliczMiesieczneOdsetki(saver2.saldo);
        System.out.println("Po 1 msc");
        System.out.println(saver1.saldo);
        System.out.println(saver2.saldo);
        RachunekBankowy.setRocznaStopaProcentowa();
        saver1.saldo=RachunekBankowy.obliczMiesieczneOdsetki(saver1.saldo);
        saver2.saldo=RachunekBankowy.obliczMiesieczneOdsetki(saver2.saldo);
        System.out.println("Po 2 msc");
        System.out.println(saver1.saldo);
        System.out.println(saver2.saldo);
    }
    
}
