package lab10_okula;

import pl.imiajd.Okula.Osoba;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;


public class OsbaTest {

    
    public static void main(String[] args) {
        LocalDate date = LocalDate.of(2001, 10, 22);
        LocalDate date2 = LocalDate.of(2000, 3, 8);
        LocalDate date3 = LocalDate.of(2023, 1, 8);
        ArrayList<Osoba> Grupa = new ArrayList<Osoba>();
        Osoba ja = new Osoba("Okula", date);
        Osoba k1 = new Osoba("Kowal", date2);  
        Osoba d1 = new Osoba("Im",LocalDate.now());
        Osoba k2 = new Osoba("Kowal", date3);
        Osoba d2 = new Osoba("Naz",LocalDate.now());
        Collections.addAll(Grupa,ja,d1,k2,k1,d2);
        System.out.println(Grupa.toString());
        Collections.sort(Grupa);
        System.out.println(Grupa.toString());
    }
    
    
}
