package lab10_okula;

import pl.imiajd.Okula.Osoba;
import pl.imiajd.Okula.Student;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;


public class StudentTest {

    
    public static void main(String[] args) {
        LocalDate date = LocalDate.of(2001, 10, 22);
        LocalDate date2 = LocalDate.of(2000, 3, 8);
        LocalDate date3 = LocalDate.of(2023, 1, 8);
        ArrayList<Osoba> Grupa = new ArrayList<Osoba>();
        Student ja = new Student(4.0,"Okula", date);
        Student k1 = new Student(2.0,"Kowal", date2);  
        Student d1 = new Student(5.0,"Im",LocalDate.now());
        Student k2 = new Student(6.0,"Kowal", date3);
        Student d2 = new Student(3.0,"Oaz",LocalDate.now());
        Collections.addAll(Grupa,ja,d1,k2,k1,d2);
        System.out.println(Grupa.toString());
        Collections.sort(Grupa);
        System.out.println(Grupa.toString());
    }
    
}
