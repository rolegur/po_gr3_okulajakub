/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.imiajd.Okula;

import java.time.LocalDate;
import java.util.Arrays;


public class Student extends Osoba implements Comparable<Osoba>,Cloneable{
    double sredniaOcen;

    public Student(double sredniaOcen, String naz, LocalDate dataUr) {
        super(naz, dataUr);
        this.sredniaOcen = sredniaOcen;
    }

    @Override
    public String toString() {
        return " nazwisko=" + nazwisko +" sredniaOcen=" + sredniaOcen +  ", dataUrodzenia=" + dataUrodzenia + ' ';
    }


    public double getSredniaOcen() {
        return sredniaOcen;
    }

    public void setSredniaOcen(double sredniaOcen) {
        this.sredniaOcen = sredniaOcen;
    }
    
        
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
    
    public int compareTo(Student o){ 
        if(this.nazwisko.equals(o.nazwisko) && this.dataUrodzenia.equals(o.dataUrodzenia)){
            if(this.sredniaOcen<o.sredniaOcen)
                 return -1;
            else if(o.sredniaOcen<this.sredniaOcen)
                return 1;
            return 0;
        }
        else if (this.nazwisko.equals(o.nazwisko)){
            return this.dataUrodzenia.compareTo(o.dataUrodzenia);
        }else
            return this.nazwisko.compareTo(o.nazwisko);
    }

    
}
