/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4_okula_zad5;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Scanner;

/**
 *
 * @author okkub
 */
public class Lab4_okula_zad5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ile masz: ");
        BigDecimal k = scanner.nextBigDecimal();
        System.out.print("Oprocentowanie w postaci 0,1=10%, 0,01=1%: ");
        BigDecimal p = scanner.nextBigDecimal();
        System.out.print("Ile lat: ");
        int n = scanner.nextInt();


          BigDecimal sumOfSaving = k;
        for(int i=0;i<n;i++){
                 
                BigDecimal sumOfYear = sumOfSaving.multiply(p);
                sumOfSaving = sumOfSaving.add(sumOfYear.setScale(2, RoundingMode.FLOOR));
           }

        System.out.println("Po " + n + " latach masz: " + sumOfSaving.setScale(2, RoundingMode.FLOOR));
    }    
}

