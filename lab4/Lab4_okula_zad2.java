/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4_okula_zad2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author okkub
 */
public class Lab4_okula_zad2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
        Scanner input = new Scanner(System.in);
        System.out.print("Podaj znak: ");
        Character character = input.next().charAt(0);

        System.out.print("Podaj nazwę pliku (z folderu src/): ");
        String fileName = input.next();

        File file = new File("src/" + fileName + ".txt");

        Scanner scan = new Scanner(file);
        String text = scan.nextLine();
         while (scan.hasNext()) {
               text += scan.nextLine();
                }
       int count=0;
       char[] napis = text.toCharArray();
       for(int i=0;i<napis.length;i++){
           if(napis[i]==character){
               count+=1;
           }
       }
        System.out.print("Znak " + character + " wystepuje w pliku " + count + " razy.");
        input.close();
    }
    
}
