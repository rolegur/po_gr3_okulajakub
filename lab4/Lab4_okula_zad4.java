/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4_okula_zad4;

import java.math.BigInteger;
import java.util.Scanner;

/**
 *
 * @author okkub
 */
public class Lab4_okula_zad4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int liczba = 1;
        BigInteger suma = BigInteger.ZERO;
        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj rozmiar: ");
        int rozm = scanner.nextInt();

        int[][] tab = new int[rozm][rozm];
        for (int i = 0; i < rozm; i++) {
            for (int j = 0; j < rozm; j++) {
                suma = suma.add(BigInteger.valueOf(liczba));
                tab[i][j] = liczba;
                liczba *= 2;
            }
        }
        System.out.println("Nasiona: " + suma);

    }
}
    

