/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4_okula;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author local
 */
public class Lab4_Okula {

   
    public static void main(String[] args) {
    
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj napis1");
        String slowo1 = scanner.next();
        System.out.println("Podaj litere");
        char c = scanner.next().charAt(0);
         System.out.println();
        System.out.println(countChar(slowo1,c));
        System.out.println("Podaj napis ktory moze zawierac sie w napisie 1");
        String slowo2 = scanner.next();
         System.out.println();
        System.out.println(countSubStr(slowo1,slowo2));
         System.out.println();
        System.out.println(middle(slowo1));
         System.out.println();
        System.out.println(repeat(slowo1,3));
         System.out.println();
        System.out.println(change("Jakies Zdanie"));
        System.out.println();
        System.out.println(nice("JakiesSlowo"));
        System.out.println(Arrays.toString(where("ala","a")));
         
           
    }
    
    static int countChar(String str,char c){
       int count=0;
       char[] napis = str.toCharArray();
       for(int i=0;i<napis.length;i++){
           if(napis[i]==c){
               count+=1;
           }
       }
    return count;
    }
    
    static int countSubStr(String str, String subStr){
        int lastIndex = 0;
        int count = 0;
        while (lastIndex != -1) {
            lastIndex = str.indexOf(subStr, lastIndex);

                if (lastIndex != -1) {
                    count++;
                    lastIndex += subStr.length();
                }  
    
            }
        return count;
    }
    
    static String middle(String str){
        int srodek=str.length()/2;
        String znak="kajaki";
        if(str.length()%2!=0){
           znak = String.valueOf(str.charAt(srodek));
        }
        else{
            znak = String.valueOf(str.charAt(srodek-1));
            znak += String.valueOf(str.charAt(srodek));     
        }    
     return znak;   
    }
    
    static String repeat(String str,int n){
        String dlugi="";
        
        for(int i=0;i<n;i++){
            dlugi += str;
        }
      return dlugi;
    }
    
    static int[] where(String str, String subStr){
        int lastIndex = 0;
        int count = 0;
       int[] List = new int[countSubStr(str,subStr)];
        while (lastIndex != -1) {
            lastIndex = str.indexOf(subStr, lastIndex);

                if (lastIndex != -1) {
                    List[count] = lastIndex;
                    count++;
                    lastIndex += subStr.length();
                }  
            }
        return List;
    }

    static String change(String str){
        StringBuffer stringBuffer = new StringBuffer(str);
        int count = 0;

        for (Character element: str.toCharArray()) {
            if(Character.isLetter(element) && Character.isLowerCase(element)){
                stringBuffer.replace(count, (count + 1),String.valueOf(Character.toUpperCase(element)));
            } else if(Character.isLetter(element) && Character.isUpperCase(element)){
                stringBuffer.replace(count, (count + 1), String.valueOf(Character.toLowerCase(element)));
            }
            count++;
        }
        return stringBuffer.toString();
    }

    static String nice(String str){
        StringBuffer strReversed = new StringBuffer(str);
        StringBuffer result = new StringBuffer();

        int counter = 0;
        for (Character element: strReversed.reverse().toString().toCharArray()) {
            if(counter == 3){
                result.append("\'").append(element);
                counter = 0;
            } else {
                result.append(element);
            }
            counter++;
        }
        return result.reverse().toString();
    }

    static String nice(String str, Character seperator, int incidence){
        StringBuffer strReversed = new StringBuffer(str);
        StringBuffer result = new StringBuffer();
        int counter = 0;
        for (Character element: strReversed.reverse().toString().toCharArray()) {
            counter++;
            if(counter == incidence){
                result.append(seperator).append(element);
                counter = 0;
            } else {
                result.append(element);
            }
        }
        return result.reverse().toString();
    }


    }