/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4_okula_zad3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author okkub
 */
public class Lab4_okula_zad3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
       Scanner input = new Scanner(System.in);
        System.out.print("Podaj slowo: ");
        String word = input.next();

        System.out.print("Podaj nazwę pliku (z folderu src/): ");
        String fileName = input.next();

        File file = new File("src/" + fileName + ".txt");

        Scanner scan = new Scanner(file);
        String text = scan.nextLine();
         while (scan.hasNext()) {
               text += scan.nextLine();
                }
          int lastIndex = 0;
        int count = 0;
        while (lastIndex != -1) {
            lastIndex = text.indexOf(word, lastIndex);

                if (lastIndex != -1) {
                    count++;
                    lastIndex += word.length();
                }  
    
            }
        System.out.print("Slowo " + word + " wystepuje w pliku " + count + " razy.");
        input.close();
    }
    
}
