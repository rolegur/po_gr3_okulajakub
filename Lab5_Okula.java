/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5_okula;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 *
 * @author local
 */
public class Lab5_Okula {


    public static void main(String[] args) {
                ArrayList lista = new ArrayList();
        ArrayList lista2 = new ArrayList();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj wielkosc listy 1: ");
        int wielkosc = scanner.nextInt();
        for(int i=0;i<wielkosc;i++){
        System.out.println("Podaj zawartosc listy 1: ");
        int zawartosc = scanner.nextInt();
        lista.add(zawartosc);
        }
        System.out.println("Podaj wielkosc listy 2: ");
        wielkosc = scanner.nextInt();
        for(int i=0;i<wielkosc;i++){
        System.out.println("Podaj zawartosc listy 2: ");
        int zawartosc = scanner.nextInt();
        lista2.add(zawartosc);
        }
        System.out.println("Append: ");
        System.out.println(append(lista, lista2));
        System.out.println();
        System.out.println("Merge: ");
        System.out.println(merge(lista, lista2)); 
        System.out.println();
        System.out.println("MergeSorted: ");
        System.out.println(mergeSorted(lista, lista2)); 
        System.out.println();
        System.out.println("Reversed: ");
        System.out.println(reversed(lista));
        System.out.println();
        reverse(lista);
        System.out.println("Reverse: ");
        System.out.println(lista);
    }
    
   public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b){
    ArrayList laczana = new ArrayList();
    for(int i=0;i<a.size();i++){
         int zaw = a.get(i);
         laczana.add(zaw);
    }
    for(int i=0;i<b.size();i++){
         int zaw = b.get(i);
         laczana.add(zaw);
     }
    return laczana;
   }
   
   public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b){
    ArrayList wynik = new ArrayList();
    if(a.size()>b.size()){
        for(int i=0;i<a.size();i++){
            if(i<b.size()){
                wynik.add(a.get(i));
                wynik.add(b.get(i));
            }
            else{
                wynik.add(a.get(i));
            }   
        }
    }
    else if(a.size()<b.size()){
        for(int i=0;i<b.size();i++){
            if(i<a.size()){
                wynik.add(a.get(i));
                wynik.add(b.get(i));
            }
            else{
                wynik.add(b.get(i));
            }   
        }  
    }
    else{
        for(int i=0;i<a.size();i++){
            wynik.add(a.get(i));
            wynik.add(b.get(i));
        }
    }
   return wynik;
  }
   
  public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b){
      ArrayList wynik = new ArrayList();
//      wynik = append(a,b);
//      Collections.sort(wynik);
//      return wynik;

        int idxa=0;
        int idxb=0;
        while (idxa < a.size() && idxb < b.size()){
            if(a.get(idxa) > b.get(idxb)){
                wynik.add(b.get(idxb));
                idxb++;
            } else {
                wynik.add(a.get(idxa));
                idxa++;
            }
        }

        while (idxa < a.size()){
            wynik.add(a.get(idxa));
            idxa++;
        }

        while (idxb < b.size()){
            wynik.add(b.get(idxb));
            idxb++;
        }
       
        return wynik;
    }
        
 
  
  
  
  public static ArrayList<Integer> reversed(ArrayList<Integer> a){
    ArrayList wynik = new ArrayList();
    for(int i=a.size()-1;i>=0;i--){
         int zaw = a.get(i);
         wynik.add(zaw);
    }
    return wynik;
  }
  
  public static void reverse(ArrayList<Integer> a){
    for(int i = 0; i < a.size()/2; i++)
    {
        int temp = a.get(a.size()-i-1);
        a.set(a.size()-i-1, a.get(i));
        a.set(i , temp);
    }
    }
  }
