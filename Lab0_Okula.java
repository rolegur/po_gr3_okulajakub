/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab0_okula;

/**
 *
 * @author local
 */
public class Lab0_Okula {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {       
        System.out.println("Zadanie pierwsze: " + zadanie1());
        System.out.println("Zadanie drugie: " + zadanie2());
        System.out.println("Zadanie trzecie: " + zadanie3());
        System.out.println("Zadanie czwarte");
         double wynik=1000;
        double dod=0;
        double procent=0.06;
        double saldo=0;
         for(int i=1; i<4; i++){
             
		  dod=wynik*procent;
                  saldo= wynik+dod;
                  wynik=saldo;
                  System.out.println("saldo po roku "+ i + " wynosi "+ saldo);
	  }
        
         System.out.println("Zadanie 5");
         zadanie5();
         System.out.println("\n");
         
         System.out.println("Zadanie 6");
         zadanie6();
         System.out.println("\n");
         
         System.out.println("Zadanie 7");
         zadanie7();
         System.out.println("\n");
         
         System.out.println("Zadanie 8");
         zadanie8();
         System.out.println("\n");
         
         System.out.println("Zadanie 9");
         zadanie9();
         System.out.println("\n");
         
         System.out.println("Zadanie 10");
         zadanie10();
         System.out.println("\n");
         
         System.out.println("Zadanie 11");
         zadanie11();
         System.out.println("\n");
         
         System.out.println("Zadanie 12");
         zadanie12();
         
    }
    static int zadanie1(){
        int wynik = 31+29+31;
        return wynik;
    }
    static int zadanie2(){
        int wynik=0;
         for(int i=1; i<=10; i++){
		  wynik=wynik+i;
	  }
        return wynik;
    }
    static int zadanie3(){
        int wynik=1;
         for(int i=1; i<=10; i++){
		  wynik=wynik*i;
	  }
        return wynik;
    }
    static void zadanie5() {
        System.out.println("+------+");
        System.out.println("| Java |");
        System.out.println("+------+");
    }
    static void zadanie6(){
        System.out.println("  /////");
        System.out.println(" +\"\"\"\"\"+");
        System.out.println("(| o o |)");
        System.out.println("  | ^ |");
        System.out.println(" | �-� |");
        System.out.println(" +-----+");
        
    }
  
    static void zadanie7(){
        System.out.println("##    ##  ##  ##   ######   ######      ####   ##   ##  ##  ##   ##       ######                   ");
        System.out.println("##   ##   ##  ##   ###  ##  ##  ##     ##  ##  ##  ##   ##  ##   ##       ##  ##         ");
        System.out.println("##  ##    ##  ##   ######   ##  ##     ##  ##  ##  ##   ##  ##   ##       ##  ##            ");
        System.out.println("#####     ##  ##   ###  ##  ######     ##  ##  #####    ##  ##   ##       ######          ");
        System.out.println("### ##    ##  ##   ##   ##  ##  ##     ##  ##  ##  ##   ##  ##   ##       ##  ##             ");
        System.out.println("##   ##    ####    ######   ##  ##      ####   ##   ##   ####    #######  ##  ##             ");

    }
    
    static void zadanie8(){
        System.out.println("   +");
        System.out.println("  + +");
        System.out.println(" +   +");
        System.out.println("+-----+");
        System.out.println("| .-. |");
        System.out.println("| | | |");
        System.out.println("+-+-+-+");
    }
    static void zadanie9(){
        System.out.println("                ___      ");
        System.out.println("      __     /       \\   ");
        System.out.println("  ___( o)>  <  hello  | ");
        System.out.println(" \\ <_. )     \\  ___  / ");
        System.out.println("  `---'             ");
        
    }
    
    static void zadanie10(){
        System.out.println("Hobbit\nIT\nHamilton");
    }
    
    static void zadanie11(){
        System.out.println("Szlachetne zdrowie,\nNikt sie nie dowie,\nJako smakujesz,\nAz sie zepsujesz.\n");
        System.out.println("Tam czlowiek prawie\nWidzi na jawie\nI sam to powie,\nZe nic nad zdrowie\nAni lepszego,\n");
        System.out.println("Ani drozszego;\nBo dobre mienie,\nPerly, kamienie,\nTakze wiek mlody\nI dar urody,\nMiesca wysokie,\nWladze szerokie Dobre sa, ale \n");
        System.out.println("Gdy zdrowie w cale.\nGdzie nie masz sily,\nI swiat niemily.\nKlinocie drogi,\nMoj dom ubogi\nOddany tobie\nUlubuj sobie!\n");
    }
    
    static void zadanie12(){
        System.out.println("******==========  ");
        System.out.println("******==========   ");
        System.out.println("******==========  ");
        System.out.println("******==========  ");
        System.out.println("================   ");
        System.out.println("================   ");
        System.out.println("================   ");
        System.out.println("================   ");
    }
}