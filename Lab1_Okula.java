/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1_okula;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author local
 */
public class Lab1_Okula {

    /**
     * @param args the command line arguments
     */
        public static void main(String[] args) {
        System.out.println("ZADANIE 1");
        System.out.println("Zadanie 1: ");
        System.out.println("Zadanie pierwsze: " + zadanie1());
        System.out.println("Zadanie 2: ");
        System.out.println("Zadanie drugie: " + zadanie2());
        System.out.println("Zadanie 3: ");
        System.out.println("Zadanie trzecie: " + zadanie3());
        System.out.println("Zadanie 4: ");
        System.out.println("Zadanie czwarte: " + zadanie4());
        System.out.println("Zadanie 5: ");
        System.out.println("Zadanie 5: " + zadanie5());
        System.out.println("Zadanie 6: ");
        System.out.println("Zadanie 6: " + zadanie6());
        System.out.println("Zadanie 7: ");
        zadanie7();
        System.out.println("Zadanie 8: ");
        System.out.println("Zadanie 8: " + zadanie8());
        System.out.println("Zadanie 9: ");
        System.out.println("Zadanie 9: " + zadanie9());
        System.out.println("Zadanie 1.2: ");
        zadanie_1();
        
        System.out.println("ZADANIE 2_1");
        System.out.println("Zadanie 2_a: " + zadanie2_a());
        System.out.println("Zadanie 2_b: " + zadanie2_b());
        System.out.println("Zadanie 2_c: " + zadanie2_c());
          System.out.println("Zadanie 2_d: " + zadanie2_d());
        System.out.println("Zadanie 2_e: " + zadanie2_e());
        System.out.println("Zadanie 2_f: " + zadanie2_f());
        System.out.println("Zadanie 2_g: " + zadanie2_g());
        System.out.println("Zadanie 2_h: " + zadanie2_h());
        

       System.out.println("Zadanie 2_4: ");
        zadanie2_4();
        System.out.println("Zadanie 2_5: ");
        System.out.println("Zadanie 2_5: " + zadanie2_5());
    }
    
    static int zadanie1(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz dodac");
        int n = scanner.nextInt();
        int wynik=0;
        for(int i=1; i<=n; i++){
            wynik += scanner.nextInt();
	  }
        return wynik;
    }
    
    static int zadanie2(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz pomnozyc");
        int n = scanner.nextInt();
        int wynik=1;
        for(int i=1; i<=n; i++){
            wynik *= scanner.nextInt();
	  }
        return wynik;
    }
    
    static int zadanie3(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz dodac z wartosci bezwzglednych");
        int n = scanner.nextInt();
        int wynik=0;
        int a;
        for(int i=1; i<=n; i++){
            a = scanner.nextInt();
            a = Math.abs(a);
            wynik+=a;
	  }
        return wynik;
    }
    
    static double zadanie4(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz dodac pod pierwiastekiem z wartosci bezwzglednych");
        int n = scanner.nextInt();
        double wynik=0;
        double a;
        for(int i=1; i<=n; i++){
            a = scanner.nextInt();
            a = Math.abs(a);
            a = Math.sqrt(a);
            wynik+=a;
	  }
        return wynik;
    }
    
    static int zadanie5(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz pomnozyc z wartosci bezwzglednych");
        int n = scanner.nextInt();
        int wynik=1;
        int a;
        for(int i=1; i<=n; i++){
            a = scanner.nextInt();
            a = Math.abs(a);
            wynik*=a;
	  }
        return wynik;
    }
    
    static int zadanie6(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz dodac kwadraty");
        int n = scanner.nextInt();
        int wynik=0;
        double a;
        for(int i=1; i<=n; i++){
            a = scanner.nextInt();
            a = Math.pow(a,2);
            wynik+=a;
	  }
        return wynik;
    }
    
     static void zadanie7(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz dodac i pomnozyc");
        int n = scanner.nextInt();
        int wynik_mn=1;
        int wynik_su=0;
        double a;
        for(int i=1; i<=n; i++){
            a = scanner.nextInt();
            wynik_su+=a;
            wynik_mn*=a;
	  }
        System.out.println("Wynik Dodawania= " + wynik_su);
        System.out.println("Wynik mnozenia= " + wynik_mn);
        
    }
    
     static int zadanie8(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz dodac na zmiane");
        int n = scanner.nextInt();
        int wynik=0;
        int a;
        for(int i=1; i<=n; i++){
            a = scanner.nextInt();
            if(i%2==0)
                a*=-1;
            wynik+=a;
	  }
        return wynik;
    }
    
     static double zadanie9(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz dodac u�amki z silnia");
        int n = scanner.nextInt();
        double wynik=0;
        double a;
        double silnia = 1;
        for(int i=1; i<=n; i++){
            a = scanner.nextInt();
            if(i%2!=0)
                a*=-1;
            silnia *= i;
            wynik+=(a/silnia);
	  }
        return wynik;
    }
     
     static void zadanie_1(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz podac");
        int n = scanner.nextInt();
        int idx;
        List<Integer> ciag = new ArrayList<Integer>();
        List<Integer> ciag2 = new ArrayList<Integer>();
        for(int i=0; i<n; i++){
            int a = scanner.nextInt();
            ciag.add(a);
            ciag2.add(a);
	  }
        
        idx = ciag.get(0);
        ciag2.add(idx);
        ciag2.remove(0);
        System.out.println(ciag);
        System.out.println(ciag2);
    }
     
     static int zadanie2_a(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz sprawdzic");
        int n = scanner.nextInt();
        int wynik=0;
        int a;
        for(int i=1; i<=n; i++){
            a = scanner.nextInt();
            if (a%2==0)
                    wynik+=1;
	  }
        return wynik;
    }
     
     static int zadanie2_b(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz sprawdzic");
        int n = scanner.nextInt();
        int wynik=0;
        int a;
        for(int i=1; i<=n; i++){
            a = scanner.nextInt();
            if (a%3==0 && a%5!=0)
                    wynik+=1;
	  }
        return wynik;
    }
     
     static int zadanie2_c(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz sprawdzic");
        int n = scanner.nextInt();
        int wynik=0;
        int a;
        for(int i=1; i<=n; i++){
            a = scanner.nextInt();
            if (Math.sqrt(a)%2==0)
                    wynik+=1;
	  }
        return wynik;
    }
     
     static int zadanie2_d(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz sprawdzic w liscie");
        int n = scanner.nextInt();
        int wynik=0;
        int a;
        List<Integer> lista = new ArrayList<Integer>();
        for(int i=0; i<n; i++){
            a = scanner.nextInt();
            lista.add(a);
	  }
        for(int i=0; i<n; i++){
            if (i==0){
                continue;
            }
            if (i==n-1){
                return wynik;
            }
            else if(lista.get(i)>(lista.get(i-1)+lista.get(i+1))/2){
                wynik+=1;
            }
        }
        return wynik;
    }
     
      static int zadanie2_e(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz sprawdzic");
        int n = scanner.nextInt();
        int wynik=0;
        int a;
        int silnia =0;
        for(int i=1; i<=n; i++){
            silnia *= i;
            a = scanner.nextInt();
            if (a>Math.pow(2,i) && a<silnia){
                wynik+=1;
            }
	  }
        return wynik;
    }
      
      static int zadanie2_f(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz sprawdzic");
        int n = scanner.nextInt();
        int wynik=0;
        int a;
        for(int i=1; i<=n; i++){
            a = scanner.nextInt();
            if (i%2!=0 && a%2==0){
                wynik+=1;
            }
	  }
        return wynik;
    }
      
      static int zadanie2_g(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz sprawdzic");
        int n = scanner.nextInt();
        int wynik=0;
        int a;
        for(int i=1; i<=n; i++){
            a = scanner.nextInt();
            if (a>0 && a%2!=0){
                wynik+=1;
            }
	  }
        return wynik;
    }
     
      static int zadanie2_h(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz sprawdzic");
        int n = scanner.nextInt();
        int wynik=0;
        int a;
        for(int i=1; i<=n; i++){
            a = scanner.nextInt();
            if (Math.abs(a)<Math.pow(i,2)){
                wynik+=1;
            }
	  }
        return wynik;
    }
      
      static int zadanie2_2(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz sprawdzic");
        int n = scanner.nextInt();
        int wynik=0;
        int a;
        for(int i=1; i<=n; i++){
            a = scanner.nextInt();
            if (a>0){
                wynik+=a;
            }
	  }
        return 2*wynik;
    }
      
       static void zadanie2_3(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz sprawdzic");
        int n = scanner.nextInt();
        int dodatnie=0;
        int ujemne =0;
        int zero = 0;
        int a;
        for(int i=1; i<=n; i++){
            a = scanner.nextInt();
            if (a>0){
                dodatnie+=1;
            }
            else if(a==0){
                zero+=1;
            }
            else if(a<0){
                ujemne+=1;
            }
	  }
        System.out.println("Dodatnie: " + dodatnie);
        System.out.println("Ujemne: " + ujemne);
        System.out.println("Zera: " + zero);
        
    }
       
       static void zadanie2_4(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz sprawdzic");
        int n = scanner.nextInt();
        List<Integer> lista2 = new ArrayList<Integer>();
        int a;
        for(int i=0; i<n; i++){
            a = scanner.nextInt();
             lista2.add(a);
        }
        int min =lista2.get(0);
        int max =lista2.get(0);
        for(int i=0; i<n; i++){
            if(min>lista2.get(i))
                min=lista2.get(i);
        }
        for(int i=0; i<n; i++){
            if(max<lista2.get(i))
                max=lista2.get(i);
        }
        System.out.println("Najwieksza: " + max);
        System.out.println("Najmniejsza: " + min);
        
    }
     
     static int zadanie2_5(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz sprawdzic");
        int n = scanner.nextInt();
        int wynik=0;
        int a;
        List<Integer> lista2 = new ArrayList<Integer>();
        for(int i=0; i<n; i++){
            a = scanner.nextInt();
             lista2.add(a);
        }    
        for(int i=0;i<n;i++){
            if(i==n-1){
                return wynik;
            }
            else if(lista2.get(i)>0 &&lista2.get(i+1)>0){
                wynik+=1;
            }
            else{
                continue;
            }
        }
        return wynik;
     }
    
}

